package com.pathmazing.tesjor.testmodule.socket;

import android.content.Context;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.engineio.client.transports.WebSocket;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.pathmazing.tesjor.testmodule.utils.Devices;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Arrays;

/**
 * Created by Danny on 10/12/2017.
 */

public abstract class TesjorSocket {
    private Socket mSocket;
    private Context context;

    public abstract String getClientID();

    private IO.Options getOptions() {
        IO.Options options = new IO.Options();
        options.forceNew = false;
        options.multiplex = false;
        options.reconnection = true;
        options.transports = new String[]{WebSocket.NAME};
        options.timeout = 5000;
        options.query = "device_id=" + Devices.getDeviceID(context) + "&client_id=" + getClientID();
        return options;
    }

    /**
     * @return name of socket we want to connect
     */
    public abstract String getSocketName();

    public Socket getmSocket() {
        return mSocket;
    }


    public TesjorSocket(Context context) {
        try {
            this.context = context;
            mSocket = IO.socket(getSocketUri(getSocketName()), getOptions());
            initSocketEvent(mSocket);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public abstract String getBaseUrl();

    private String getSocketUri(String name) {
        return getBaseUrl() + name;
    }

    public void connect() {
        mSocket.connect();
    }

    public void disConnect() {
        mSocket.disconnect();
    }

    public void initSocketEvent(final Socket socket) {
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("Socket " + getSocketName(), "EVENT_CONNECT : " + Arrays.toString(args));
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("Socket " + getSocketName(), "EVENT_DISCONNECT : " + Arrays.toString(args));
            }
        }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("Socket " + getSocketName(), "EVENT_CONNECT_ERROR : " + Arrays.toString(args));
                connect();
            }
        }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("Socket " + getSocketName(), "EVENT_ERROR : " + Arrays.toString(args));
                connect();
            }
        }).on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("Socket " + getSocketName(), "EVENT_CONNECT_TIMEOUT : " + Arrays.toString(args));
                connect();
            }
        }).on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("Socket " + getSocketName(), "EVENT_RECONNECT : " + Arrays.toString(args));
            }
        });
    }

    public void onReceiveEvent(String eventName, JSONObject data) {

    }
}
