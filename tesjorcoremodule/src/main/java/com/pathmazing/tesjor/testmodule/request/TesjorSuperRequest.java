package com.pathmazing.tesjor.testmodule.request;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.pathmazing.tesjor.testmodule.utils.Devices;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Kevin on 9/26/2016.
 */

public abstract class TesjorSuperRequest<T> extends SuperRequest<T> {

    private String AcceptLanguage = "en";
    private String ContentType = "application/json; charset=utf-8";
    private int method = Request.Method.GET;

    public abstract String getLanguageCode();

    public TesjorSuperRequest(Context context) {
        super(context);
    }

    /**
     * default GET
     */
    @Override
    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    @Override
    public abstract String getBaseUrl();

    public abstract String getBaseController();

    @Override
    public Map<String, String> onCreateHeader(Map<String, String> header) {
        header = new HashMap<>();
        header.put("Device-Name", Devices.getDeviceName());
        header.put("Ud-Id", Devices.getDeviceID(getContext()));
        header.put("Platform", "Android");
        header.put("Model", Devices.getDeviceModel());
        header.put("Language-code", getLanguageCode());
        header.put("Os-Version", Devices.getDeviceOSVersion());
        header.put("App-Version", Devices.getAppVersionName(getContext()));
        header.put("Accept-Language", getAcceptLanguage());
        header.put("App-Type", "client");
        if (getContentType() != null) {
            header.put("Content-Type", getContentType());
        }
        header.put("Local-Timezone", getLocalTimezone());
        header.putAll(getAuthHeader());
        Log.d("HEADER:>>>", this.getClass().getSimpleName() + "(" + getContext().getClass().getSimpleName() + ") : " + header.toString());
        return header;
    }

    public String getLocalTimezone() {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        return tz.getID();
    }

    public String getAcceptLanguage() {
        return AcceptLanguage;
    }

    public void setAcceptLanguage(String acceptLanguage) {
        AcceptLanguage = acceptLanguage;
    }

    public String getContentType() {
        return ContentType;
    }

    public void setContentType(String contentType) {
        ContentType = contentType;
    }

    public abstract String getFunctionName();

    @Override
    public void execute() {
        super.execute();
        try {
            Log.d("EXECUTE:>>>", this.getClass().getSimpleName() + "(" + getContext().getClass().getSimpleName() + ") : " + getRequestUrl());
            if (!TextUtils.isEmpty(onGetBodyRequest()))
                Log.d("EXECUTE:>>>", this.getClass().getSimpleName() + " JSON data " + new JSONObject(onGetBodyRequest()).toString(1));
            else
                Log.d("EXECUTE:>>>", this.getClass().getSimpleName() + " JSON data null");
        } catch (Exception e) {
        }
    }

    public static String getErrorMessageFrom(VolleyError volleyError, String defaultMsg) {
        if (volleyError instanceof NoConnectionError)
            return "No internet connection!";
        else if (volleyError instanceof TimeoutError)
            return "Check your internet connection and try again!";
        try {
            return new JSONObject(new String(volleyError.networkResponse.data)).optString("message", defaultMsg);
        } catch (Exception e) {
            try {
                String msg = new String(volleyError.networkResponse.data);
                if (TextUtils.isEmpty(msg)) {
                    msg = defaultMsg;
                }
                return msg;
            } catch (Exception ee) {
                return defaultMsg;
            }
        }
    }

    public static String getErrorMessageFrom(VolleyError volleyError) {
        return getErrorMessageFrom(volleyError, "");
    }

    public abstract Map<String, String> getAuthHeader();
}
