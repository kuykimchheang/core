/*
 * @(#)AppSharedpreferences.java 1.0
 *
 * Copyright
 *
 *
 */
package com.pathmazing.tesjor.testmodule.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

public class TesjorSharedPreferences {

    private static final String APP_SHARED_PREFS = "TesjorApp";
    private SharedPreferences _sharedPrefs;
    private Editor _prefsEditor;
    private static TesjorSharedPreferences mAppShareConstant;

    public enum SharedPreKeyType {
        CODE_TABLE_STATUS,
        LONGITUDE,
        LATITUDE,
        MAP_STATUS,
        PUSH_TOKEN_GCM,
    }

    private TesjorSharedPreferences(Context context) {
        this._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this._prefsEditor = _sharedPrefs.edit();
    }

    public synchronized static TesjorSharedPreferences getConstant(Context context) {
        if (null == mAppShareConstant) {
            mAppShareConstant = new TesjorSharedPreferences(context);
        }
        return mAppShareConstant;
    }

    /**
     * @param authorizeModel
     */
    public void setAuthorize(Authorize authorizeModel, SharedPreKeyType sharedPreKeyType) {
        Gson json = new Gson();
        String authJSON = json.toJson(authorizeModel);
        _prefsEditor.putString(sharedPreKeyType.name(), authJSON);
        _prefsEditor.apply();
    }

    /**
     * @return
     */
    public Authorize getAuthorize(SharedPreKeyType sharedPreKeyType) {
        Gson gson = new Gson();
        String json = _sharedPrefs.getString(sharedPreKeyType.name(), "");
        return gson.fromJson(json, Authorize.class);
    }

    /**
     * @param codeTableStatus
     */
    public void setCodeTableStatus(String codeTableStatus) {
        _prefsEditor.putString(SharedPreKeyType.CODE_TABLE_STATUS.toString(), codeTableStatus);
        _prefsEditor.apply();
    }

    /**
     * @return
     */
    public String getCodeTableStatus() {
        return _sharedPrefs.getString(SharedPreKeyType.CODE_TABLE_STATUS.toString(), "");
    }

    /**
     * @param tokenGCM
     */
    public void setTokenGCM(String tokenGCM) {
        _prefsEditor.putString(SharedPreKeyType.PUSH_TOKEN_GCM.toString(), tokenGCM);
        _prefsEditor.apply();
    }

    /**
     * @return
     */
    public String getTokenGCM() {
        return _sharedPrefs.getString(SharedPreKeyType.PUSH_TOKEN_GCM.toString(), null);
    }

    public void clearSharedPreference(SharedPreKeyType sharedPreKeyType) {
        _prefsEditor.remove(sharedPreKeyType.name());
        _prefsEditor.apply();
    }

    public void clearAll() {
        _prefsEditor.clear().apply();
    }

    /**
     * Get Latitude ic_location.
     *
     * @return
     */
    public String getLatitude() {
        return _sharedPrefs.getString(SharedPreKeyType.LATITUDE.toString(), "");
    }

    /**
     * Set Latitude ic_location.
     *
     * @param latitude
     */
    public void setLatitude(String latitude) {
        _prefsEditor.putString(SharedPreKeyType.LATITUDE.toString(), latitude);
        _prefsEditor.apply();
    }

    /**
     * Get Longitude ic_location.
     *
     * @return
     */
    public String getLongitude() {
        return _sharedPrefs.getString(SharedPreKeyType.LONGITUDE.toString(), "");
    }

    /**
     * Set Longitude ic_location.
     *
     * @param longitude
     */
    public void setLongitude(String longitude) {
        _prefsEditor.putString(SharedPreKeyType.LONGITUDE.toString(), longitude);
        _prefsEditor.apply();
    }

    /**
     * set Map Status
     *
     * @param status
     */
    public void setMapStatus(String status) {
        _prefsEditor.putString(SharedPreKeyType.MAP_STATUS.toString(), status);
        _prefsEditor.apply();
    }

    /**
     * get Map Status
     *
     * @return
     */
    public String getMapStatus() {
        return _sharedPrefs.getString(SharedPreKeyType.MAP_STATUS.toString(), "");
    }
}
